<?php
session_start();
include('../include/menu.php');
require_once('../include/connexion.php');
require_once('../include/fonction.php');
echo afficheMessages();
?>

<?php
$id = (isset($_GET['id'])) ? $_GET['id'] : 0;
if ($id == 0) {
  header("Location:$url/listefournisseur.php");
  die();
}

// Parti modifier
if (isset($_POST['Modifier'])) {


  if (empty($_POST['adresse1'])) {
    $_SESSION['MSG_KO'] = "L'adresse est obligatoire";
  }
  if (strlen($_POST['nom']) < 3 || empty($_POST['nom'])) {
    $_SESSION['MSG_KO'] = "Minimum 3 caracteres !";
  }


  if (!isset($_SESSION['MSG_KO'])) {

    try {
      $requete = $bdd->prepare('update fournisseur
          set nom = :nom
          , adresse1 = :adresse1
          , adresse2 = :adresse2
          , ville = :ville
          , contact = :contact
          , civilite = :civilite
          where code = :code');
      $requete->execute(array(
        'nom' => $_POST['nom'], 'adresse1' => $_POST['adresse1'], 'adresse2' => $_POST['adresse2'], 'ville' => $_POST['ville'], 'contact' => $_POST['contact'], 'civilite' => $_POST['civilite'], 'code' => $_POST['code']
      ));
      $_SESSION['MSG_OK'] = "Modification bien enregistrée";
    } catch (PDOException $e) {
      echo "Erreur !: " . $e->getMessage() . "<br/>";
      die();
    }
  }
}


try {
  $requete = $bdd->prepare('select nom, adresse1, adresse2, ville, contact,
civilite, code from fournisseur where code = ?');
  $requete->execute(array($id));
  $fournisseur = $requete->fetch();
} catch (PDOException $e) {
  print "Erreur !: " . $e->getMessage() . "<br/>";
  die();
}
// Condition pour vérifier si le bouton Annuler a été cliqué
if (isset($_POST['Annuler'])) {
  // Redirection vers la liste des fournisseurs
  header('Location: listefournisseurs.php');
  exit(); // Terminer le script pour éviter toute exécution ultérieure
}
//suppression d'un fournisseur
if (isset($_POST['Supprimer'])) {
  try {
    $requete = $bdd->prepare('delete from fournisseur where code = ?');
    $requete->execute(array($_POST['code']));
    $_SESSION['MSG_OK'] = "supression bien enregistrée";
    header("Location: ./listefounisseur.php");
    exit;
  } catch (PDOException $e) {
    print "Erreur !:" . $e->getMessage() . "<br/>";
    echo afficheMessages($_SESSION['MSG_KO'] = "Modification non enregistrée");
    die();
  }
}
/* Vérification de l'unicité du nom du fournisseur
        $requete = $bdd->prepare('SELECT COUNT(*) AS cpt FROM fournisseur WHERE nom = ?');
        $requete->execute(array($nom));
        $compteur = $requete->fetch();
        if ($compteur['cpt'] == 1) {
            $retour .= "le nom (" . $_POST['nom'] . ") est déjà pris<br />";
        }*/
?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>fournisseur <?php echo $fournisseur['nom']; ?></title>
  <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/style.css" rel="stylesheet">
</head>

<body>
  <div class="container">
    <h1>Fournisseur : <?php echo $fournisseur['nom']; ?></h1>

    <form method="post">
      <!-- code fournisseur -->
      <input type="hidden" class="form-control" id='code' name='code' value="<?php echo $fournisseur['code'] ?>">

      <div class="mb">
        <label for="nom" class="form-label">Nom : </label>
        <input type="text" class="form-control" id='nom' name="nom" aria-describedby="emailHelp" value="<?php echo $fournisseur['nom'] ?>">
      </div>

      <div class="mb">
        <label for="adresse1" class="form-label">Address 1 : </label>
        <input type="text" class="form-control" id='ad1' name='adresse1' aria-describedby="emailHelp" value="<?php echo $fournisseur['adresse1'] ?>">
      </div>
      <div class="mb">
      </div>
      <div class="mb">
        <label for="adresse2" class="form-label">Address 2 : </label>
        <input type="text" class="form-control" id="ad2" name='adresse2' aria-describedby="emailHelp" value="<?php echo $fournisseur['adresse2'] ?>">
      </div>
      <div class="mb">

        <div class="mb">
          <label class="ville" id='ville' name='ville' for="ville">Ville : </label>
          <div class="col-sm-10">
            <?php echo selectVille('ville', $fournisseur['ville']); ?>
          </div>
        </div>

      </div>
      <div class="mb">
        <label for="contact" class="form-label">Contact : </label>
        <input type="text" class="form-control" id="contact" name='contact' aria-describedby="emailHelp" value="<?php echo $fournisseur['contact'] ?>">
      </div>

      <div class="formulaire">
        <div class="form-group row">
          <label class="civilite" id='civilite' name='civilite' for="civilite">Civilité</label>
          <div class="col-sm-10">
            <?php echo selectCivilite('civilite', $fournisseur['civilite']); ?>
          </div>

        </div>
        <div class="form-group row float-right">
          <input type="submit" class="btn btn-default" name="Annuler" value="Annuler">
          <input type="submit" class="btn btn-primary" name="Modifier" value="Modifier">
        </div>
        <br> <button type="submit" name="Supprimer" class="btn btn-danger confirm">Supprimer</button></br>
      </div>
    </form>
  </div>
</body>
<script src="../node_modules/jquery/dist/jquery.js"></script>
<script>
  $(function() {
    $('.confirm').click(function() {
      return window.confirm("Êtes-vous sur ?");
    });
  });
</script>

</html>